<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    protected $table = 'gurus';
    protected $primaryKey = 'id_guru';
    protected $fillable = [
        'nama', 'nip', 'username', 'password', 'jabatan', 'status', 'admin_id'
    ];
}
