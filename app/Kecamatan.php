<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    protected $table = 'kecamatans';
    protected $primaryKey = 'id_kecamatan';
    protected $fillable = [
        'admin_id', 'nama', 'keterangan'
    ];

    public function sekolah(){
        return $this->hasOne('App\Sekolah');
    }
}
