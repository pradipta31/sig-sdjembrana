<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Session;
use App\Admin;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function proccessLogin(Request $r){
        $username = $r->username; // buat variabel untuk nyimpen username
        $password = $r->password; // buat variabel untuk nyimpen username
        $data = Admin::where('username', $username)->first(); // select ke tabel admin di database apakah username tersebut ada atau tidak
        if ($data) { // jika username tersebut ada
            if ($data->status == '1') { // jika status admin nya aktif
                if(Hash::check($password,$data->password)){ 
                    Session::put('id_admin', $data->id_admin);
                    Session::put('logged_in',TRUE);
                    return redirect(url('admin'));
                }       
                else{
                    Session::flash('error', 'Login Gagal! Username atau Password Salah!');
                    return redirect()->back()->withInput();
                }
            }else{
                Session::flash('error', 'Login Gagal! Admin nonaktif!');
                return redirect()->back()->withInput();
            }
        }else{
            Session::flash('error', 'Login Gagal! Username atau Password Salah!');
            return redirect()->back()->withInput();
        }
    }
    public function logout(){
        Session::flush();
        return redirect('login')->with('alert','Kamu sudah logout');
    }

}
