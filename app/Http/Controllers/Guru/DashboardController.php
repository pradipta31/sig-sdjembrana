<?php

namespace App\Http\Controllers\Guru;

use Session;
use App\Kecamatan;
use App\Sekolah;
use App\Guru;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index(){
        if (Session::get('log_in') == TRUE) {
            $countPane = [
                'guru' => Guru::where('id_guru',Session::get('id_guru'))->first(),
                'teachers' => Guru::all()->count(),
                'districts' => Kecamatan::all()->count(),
                'schools' => Sekolah::all()->count()
            ];
            
            return view('admin.dashboard', $countPane);
        }else{
            return redirect('guru');
        }
    }
}
