<?php

namespace App\Http\Controllers\Frontend;

use App\Sekolah;
use App\Kecamatan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index(){
        $districts = Kecamatan::all();
        return view('home', compact('districts'));
    }
}
