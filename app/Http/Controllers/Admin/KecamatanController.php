<?php

namespace App\Http\Controllers\Admin;

use Session;
use Validator;
use App\Admin;
use App\Kecamatan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class KecamatanController extends Controller
{
    public function index(){
        if (Session::get('logged_in') == TRUE) {
            $admin = Admin::where('id_admin',Session::get('id_admin'))->first();
            $districts = Kecamatan::all();
            return view('admin.kecamatan.index', compact('admin', 'districts'));
        }else{
            return redirect('login');
        }
    }

    public function store(Request $r){
        $validator = Validator::make($r->all(),[
            'nama' => 'required',
            'keterangan' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $nama = Kecamatan::where('nama', $r->nama)->first();
            if ($nama == null) {
                $kecamatan = Kecamatan::create([
                    'nama' => $r->nama,
                    'keterangan' => $r->keterangan,
                    'admin_id' => Session::get('id_admin')
                ]);
                toastSuccess('Data kecamatan baru berhasil ditambah!');
                return redirect()->back();
            }else{
                toastError('Data kecamatan '.$r->nama.' sudah ada!');
                return redirect()->back()->withInput();
            }
        }
    }

    public function update(Request $r, $id){
        $validator = Validator::make($r->all(),[
            'nama' => 'required',
            'keterangan' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $kecamatan = Kecamatan::where('id_kecamatan',$id)->update([
                'nama' => $r->nama,
                'keterangan' => $r->keterangan
            ]);
            toastSuccess('Data kecamatan berhasil diubah!');
            return redirect()->back();
        }
    }

    public function destroy($id){
        $kecamatan = Kecamatan::where('id_kecamatan',$id)->delete();
        toastSuccess('Data kecamatan berhasil dihapus!');
        return redirect()->back();
    }
}
