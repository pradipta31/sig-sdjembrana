<?php

namespace App\Http\Controllers\Admin;

use Session;
use App\Guru;
use App\Kecamatan;
use App\Sekolah;
use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index(){
        if (Session::get('logged_in') == TRUE) {
            $countPane = [
                'admin' => Admin::where('id_admin',Session::get('id_admin'))->first(),
                'teachers' => Guru::all()->count(),
                'districts' => Kecamatan::all()->count(),
                'schools' => Sekolah::all()->count()
            ];
            
            return view('admin.dashboard', $countPane);
        }else{
            return redirect('login');
        }
    }
}
