<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'nama' => 'ana',
            'nip'  => '12345',
            'username' => 'ana',
            'password' => bcrypt('123'),
            'status' => '1',
            'created_at'=> now(),
            'updated_at'=> now(),
        ]);
    }
}
