<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGurusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gurus', function (Blueprint $table) {
            $table->bigIncrements('id_guru');
            $table->string('nama',50);
            $table->string('nip',25);
            $table->string('username',25);
            $table->string('password',60);
            $table->string('jabatan', 50);
            $table->integer('status');
            $table->unsignedBigInteger('admin_id')
                    ->foreign('admin_id')->references('id_admin')->on('admins');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gurus');
    }
}
