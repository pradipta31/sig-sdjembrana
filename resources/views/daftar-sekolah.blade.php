@extends('home')
@section('cssend')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <script src="http://maps.google.com/maps/api/js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.24/gmaps.js"></script>
@endsection
@section('confront')
    <style>
        .btn_style{
            padding: 7px 25px;
            border-radius: 28px;
            border: 1px;
            background-color: #5777ba;
            color: azure;
            text-shadow: 1px;
        }
        #mymap {
            border:1px solid black;
      		width: 1080px;
      		height: 500px;
        }
        .warna_tulisan{
            color:white
        }
    </style>
    <section id="pricing" class="pricing">
        <div class="container">
            <hr>
            <div class="section-title">
                <h2>Daftar Sekolah</h2>
                <p>Cari sekolah berdasarkan kecamatan. </p>
                
                    
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-4">
                        <form action="{{url('search')}}" method="GET">
                            <div class="row">
                                <div class="col-lg-7">
                                    <div class="form-group">
                                        <select name="id_kecamatan" class="form-control">
                                            <option value="all">All</option>
                                            @foreach ($districts as $district)
                                                <option value="{{$district->id_kecamatan}}">{{$district->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <input type="submit" value="Cari" class="btn_style">
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-lg-4">
                        <form action="{{url('cari')}}" method="GET" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-lg-7">
                                    <div class="form-group">
                                        <input type="text" name="cari" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <input type="submit" value="Cari" class="btn_style">
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-lg-2"></div>

                    <div class="col-lg-12">
                        <div id="mymap"></div>
                    </div>
                </div>
                    
            </div>
  
        </div>
      </section>

    <footer id="footer">
        @include('searchmap')
    </footer>
@endsection
@section('jsend')
<script src="{{asset('frontend/bootbox/bootbox.min.js')}}"></script>
<script type="text/javascript">


    var locations = {!! json_encode($locations->toArray()) !!};

    console.log(locations);
    var mymap = new GMaps({
      el: '#mymap',
      lat: -8.3131539,
      lng: 114.5427698,
      zoom:10
    });


    $.each( locations, function( index, value ){
	    mymap.addMarker({
	      lat: value.lat,
	      lng: value.long,
	      title: value.nama_sekolah,
          icon: "https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png",
	      click: function(e) {
	        bootbox.alert({
                title: value.nama_sekolah,
                message: value.alamat_sekolah+'<br><a href="'+value.lokasi+'" target="_blank"> Menuju Google Map</a>',
            });
	      }
	    });

   });


  </script>

@endsection