@extends('admin.layouts.master',['activeMenu' => 'tambah-sekolah'])
@section('title', 'Tambah Sekolah')
@section('breadcrumb', 'Tambah Sekolah')
@section('css')
    <script src="http://maps.googleapis.com/maps/api/js"></script>
    <script>
        // variabel global marker
        var marker;
          
        function taruhMarker(peta, posisiTitik){
            
            if( marker ){
              // pindahkan marker
              marker.setPosition(posisiTitik);
            } else {
              // buat marker baru
              marker = new google.maps.Marker({
                position: posisiTitik,
                map: peta
              });
            }
          
             // isi nilai koordinat ke form
            document.getElementById("lat").value = posisiTitik.lat();
            document.getElementById("lng").value = posisiTitik.lng();
            
        }
          
        function initialize() {
          var propertiPeta = {
            center:new google.maps.LatLng(-8.5830695,116.3202515),
            zoom:9,
            mapTypeId:google.maps.MapTypeId.ROADMAP
          };
          
          var peta = new google.maps.Map(document.getElementById("googleMap"), propertiPeta);
          
          // even listner ketika peta diklik
          google.maps.event.addListener(peta, 'click', function(event) {
            taruhMarker(this, event.latLng);
          });
        
        }
        
        
        // event jendela di-load  
        google.maps.event.addDomListener(window, 'load', initialize);
          
        
        </script>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Tambah Data Sekolah Baru</h3>
                </div>
                <form role="form" action="{{url('admin/sekolah/tambah')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label>Kecamatan</label>
                            <select name="kecamatan_id" class="form-control" value="{{old('kecamatan_id')}}">
                                @foreach($kecamatans as $kecamatan)
                                    <option value="{{$kecamatan->id_kecamatan}}">{{$kecamatan->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nama Sekolah</label>
                            <input type="text" class="form-control" placeholder="Masukan Nama Sekolah" name="nama_sekolah" value="{{old('nama_sekolah')}}">
                        </div>
                        <div class="form-group">
                            <label>Alamat Sekolah</label>
                            <input type="text" class="form-control" name="alamat_sekolah" value="{{old('alamat_sekolah')}}" placeholder="Masukan alamat sekolah">
                        </div>
                        <div class="form-group">
                            <label>Latitude</label>
                            <input type="text" class="form-control" name="lat" id="lat" value="{{old('lat')}}">
                        </div>
                        <div class="form-group">
                            <label>Longitude</label>
                            <input type="text" class="form-control" name="long" id="lng" value="{{old('long')}}">
                        </div>
                        <div class="form-group">
                            <label>Lokasi</label>
                            <input type="text" class="form-control" name="lokasi" value="{{old('lokasi')}}">
                            <small>NB: Copy link lokasi map dari google.</small>
                        </div>
                        <div class="form-group">
                            <label>Keterangan</label>
                            <textarea name="keterangan" class="form-control" cols="30" rows="5">{{old('keterangan')}}</textarea>
                        </div>

                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-save"></i>
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-6">
            <div id="googleMap" style="width:100%;height:380px;"></div>
        </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript">
        $('#password, #confirmation_password').on('keyup', function () {
            if ($('#password').val() == $('#confirmation_password').val()) {
                $('#message').html('Password dapat digunakan!').css('color', 'green');
            } else {
                $('#message').html('Password tidak sama!').css('color', 'red');
            }
        });
    </script>
@endsection