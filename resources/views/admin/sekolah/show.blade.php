<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Data Map Sekolah</title>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
  </head>
  <body>
    
    <!-- elemen untuk menampilkan peta -->
    <div id="map"></div>
    
    
    <script>
    var arr = {!! json_encode($marker->toArray()) !!};
    var lat = arr.lat;
    var long = arr.long;
    var nama = arr.nama_sekolah;
      function initMap() {
        
        // membuat objek untuk titik koordinat
        // var lokasi = {lat: lat, lng: long};
        // console.log(lt);
        // membuat objek peta
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 9,
          center: new google.maps.LatLng(lat,long),
          icon: "https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png"
        });

        // mebuat konten untuk info window
        var contentString = '<h3>'
        +arr.nama_sekolah+
        '<p>'+arr.alamat_sekolah+
        '<br><a href="'+arr.lokasi+'" target="_blank">Klik untuk lihat Lokasi</a>'
        '</p></h3>';

        // membuat objek info window
        var infowindow = new google.maps.InfoWindow({
          content: contentString,
          position: new google.maps.LatLng(lat,long),
        });
        
        // membuat marker
        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(lat,long),
          map: map,
          title: nama,
          icon: "https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png"
        });
        
        // event saat marker diklik
        marker.addListener('click', function() {
          // tampilkan info window di atas marker
          infowindow.open(map, marker);
        });
        
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?callback=initMap"></script>
  </body>
</html>