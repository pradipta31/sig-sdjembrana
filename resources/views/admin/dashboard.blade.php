@extends('admin.layouts.master',['activeMenu' => 'home'])
@section('title', 'Dashboard')
@section('breadcrumb', 'Dashboard Panel')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-success" role="alert">
                <h4 class="alert-heading">Selamat Datang Di Website Sistem Informasi Geografis Pemetaan Sekolah Dasar.</h4>
                <p>Anda telah sukses memasukkan username dan password.</p>
                <hr>
                <p class="mb-0"></p>
            </div>
        </div>
        <div class="col-lg-4 col-6">
            <div class="small-box bg-info">
                <div class="inner">
                    <h3>{{$teachers}}</h3>

                    <p>Total Guru</p>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
                <a href="{{url('admin/guru')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-4 col-6">
            <div class="small-box bg-success">
                <div class="inner">
                    <h3>{{$districts}}</h3>

                    <p>Total Kecamatan</p>
                </div>
                <div class="icon">
                    <i class="fa fa-location"></i>
                </div>
                <a href="{{url('admin/kecamatan')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        
        <div class="col-lg-4 col-6">
            <div class="small-box bg-warning">
                <div class="inner">
                    <h3>{{$schools}}</h3>

                    <p>Total Sekolah</p>
                </div>
                <div class="icon">
                    <i class="fa fa-school"></i>
                </div>
                <a href="{{url('admin/sekolah')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>
@endsection