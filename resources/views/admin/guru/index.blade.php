@extends('admin.layouts.master',['activeMenu' => 'data-guru'])
@section('title', 'Data Guru')
@section('breadcrumb', 'Data Guru')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data Guru</h3>
                </div>
                <div class="card-body">
                    <table id="tableGuru" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>NIP</th>
                                <th>Username</th>
                                <th>Jabatan</th>
                                <th>Status</th>
                                <th>Tanggal</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($gurus as $guru)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{$guru->nama}}</td>
                                    <td>{{$guru->nip}}</td>
                                    <td>{{$guru->username}}</td>
                                    <td>{{$guru->jabatan}}</td>
                                    <td>
                                        @if($guru->status == 1)
                                            <span class="lb success">Aktif</span>
                                        @else
                                            <span class="lb warning">Non Aktif</span>
                                        @endif
                                    </td>
                                    <td>
                                        {{$guru->created_at->format('d-m-Y')}}
                                    </td>
                                    <td>
                                        <a href="javascript:void(0)" class="btn btn-info btn-sm" data-toggle="modal" data-target="#editGuru{{$guru->id_guru}}">
                                            <i class="fa fa-pencil"></i>
                                            Edit
                                        </a>
                                        <a href="javascript:void(0)" class="btn btn-danger btn-sm" onclick="deleteGuru('{{$guru->id_guru}}')">
                                            <i class="fa fa-trash"></i>
                                            Hapus
                                        </a>
                                    </td>
                                </tr>

                                <div class="modal fade" id="editGuru{{$guru->id_guru}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h3 class="modal-title" id="exampleModalLabel">Edit Data Guru : <br>{{$guru->nama}}</h3>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form action="{{url('admin/guru/'.$guru->id_guru.'/edit')}}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                <input type="hidden" name="_method" value="put">
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="col-form-label">Nama Guru</label>
                                                                <input type="text" class="form-control" name="nama" placeholder="Masukan Nama Guru" value="{{ $guru->nama }}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="col-form-label">NIP</label>
                                                                <input type="text" class="form-control" name="nip" placeholder="Masukan NIP" value="{{ $guru->nip }}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="col-form-label">Username</label>
                                                                <input type="text" class="form-control" name="username" value="{{ $guru->username }}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label>Jabatan</label>
                                                                <select name="jabatan" class="form-control" value="{{$guru->jabatan}}">
                                                                    <option value="">Pilih Jabatan</option>
                                                                    <option value="Kepala Sekolah" {{$guru->jabatan == 'Kepala Sekolah' ? 'selected' : ''}}>Kepala Sekolah</option>
                                                                    <option value="Wakil Kepala Sekolah" {{$guru->jabatan == 'Wakil Kepala Sekolah' ? 'selected' : ''}}>Wakil Kepala Sekolah</option>
                                                                    <option value="Tata Usaha" {{$guru->jabatan == 'Tata Usaha' ? 'selected' : ''}}>Tata Usaha</option>
                                                                    <option value="Guru" {{$guru->jabatan == 'Guru' ? 'selected' : ''}}>Guru</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="col-form-label">Status</label>
                                                                <select name="status" class="form-control" value="{{$guru->status}}">
                                                                    <option value="1" {{$guru->status == '1' ? 'selected' : ''}}>Aktif</option>
                                                                    <option value="0" {{$guru->status == '0' ? 'selected' : ''}}>Nonaktif</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="javascript:void(0);" onclick="updatePassword('{{$guru->id_guru}}')" class="btn btn-default">Ganti Password</a>
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <form class="hidden" action="" method="post" id="formPassword">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put">
        <input type="hidden" name="password" value="" id="password">
    </form>
    <form class="hidden" action="" method="post" id="formDelete">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="delete">
    </form>
@endsection
@section('js')
    <script src="{{asset('backend/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script src="{{asset('backend/bootbox/bootbox.min.js')}}"></script>
    <script type="text/javascript">
        $(function () {
            $('#tableGuru').DataTable();
        });

        function editGuruModal(){
            $('#editGuru').modal('show');
        }

        function updatePassword(id){
            bootbox.prompt({
                title: 'Masukan password baru.',
                inputType: 'password',
                size: 'small',
                callback: function(result){
                    if (result != null ) {
                        bootbox.prompt({
                        title: 'Masukan password kembali.',
                        inputType: 'password',
                        size: 'small',
                            callback: function(password){
                                if (password != null) {
                                    if (result == password) {
                                        $('#password').val(password);
                                        $('#formPassword').attr('action', '{{url('admin/guru/password')}}/'+id);
                                        $('#formPassword').submit();
                                    }else {
                                        bootbox.alert("Password tidak sama. Silakan ulangi !");
                                    }
                                }
                            }
                        });
                    }
                }
            });
        }

        function deleteGuru(id){
            swal({
                title: "Anda yakin?",
                text: "Anda tidak dapat mengembalikan data yang sudah terhapus!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Berhasil! Data guru yang anda pilih berhasil dihapus!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formDelete').attr('action', '{{url('admin/guru/delete')}}/'+id);
                        $('#formDelete').submit();
                    }); 
                }
            });
        }
    </script>
@endsection