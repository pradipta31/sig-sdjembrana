@extends('layouts.master')
@section('confront')
    @include('layouts.carousel')
    @include('layouts.features')
    <footer id="footer">
        @include('searchmap')
    </footer>
@endsection