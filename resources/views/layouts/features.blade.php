<!-- ======= Frequently Asked Questions Section ======= -->
<section id="faq" class="faq section-bg">
  <div class="container">

    <div class="section-title">

      <h2>Cari Sekolah Berdasar Kecamatan</h2>
      <p>Cari sekolah berdasarkan kecamatan se-kabupaten jembrana.</p>
    </div>

    <div class="accordion-list">
      <ul>
        @foreach ($districts as $item)
          <li data-aos="">
            <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" class="collapse" href="#accordion-list-{{$item->id_kecamatan}}" style="font-size: 1rem">Kecamatan {{$item->nama}} <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
            <div id="accordion-list-{{$item->id_kecamatan}}" class="collapse" data-parent=".accordion-list">
              <hr>
              <p>
                @php
                    $schools = \App\Sekolah::where('kecamatan_id',$item->id_kecamatan)->get();
                    $no = 1;
                @endphp
                @foreach ($schools as $sekolah)
                    <span style="font-family:'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif">
                      {{$no++}}.
                      {{$sekolah->nama_sekolah}}
                      <a href="{{url('sekolah/'.$sekolah->id_sekolah)}}" style="display:unset">
                        Lihat
                        <i class="bx bx-right-arrow-circle"></i>
                      </a>
                    </span>
                    <hr>
                @endforeach
              </p>
            </div>
          </li>
        @endforeach
      </ul>
    </div>

  </div>
</section><!-- End Frequently Asked Questions Section -->